#! /usr/bin/env python3

DOCKER_CONTAINER_ID = 'fb89a0e33bc1'
SERVER = 'localhost:2700'
AUDIO_DIR = '~/Recordings'

import os
import fcntl
import pathlib
import argparse
import subprocess
import asyncio
import threading
import queue
import websockets
import tkinter
import tkinter.scrolledtext
import tkinter.messagebox
import tkinter.ttk
import tkinter.font
import tkinter.filedialog
import json
import logging
import re

from pyaudio import PyAudio, Stream, paInt16
from contextlib import asynccontextmanager, contextmanager, AsyncExitStack
from typing import AsyncGenerator, Generator

class Reco(threading.Thread):

    FORMAT = paInt16
    CHANNELS = 1
    RATE = 16000
    CHUNK = 16000

    def __init__(self, logger,  name='Voice Recognition thread', server='localhost:2700'):
        threading.Thread.__init__(self, name=name)
        self.server = server
        # The queue is a queue.Queue object that is used to communicate with tkinter application
        self.queue = queue.Queue()
      # The shutdown_flag is a threading.Event object that indicates whether the thread should be terminated.
        self.shutdown_flag = threading.Event()
        # Use logger
        self.logger = logger
        
    def cleanly_stop(self):
        # stop thread
        self.shutdown_flag.set()
        self.join()
        ret = self.cleanly_empty_queue()
        self.queue.join()
        return ret

    def cleanly_empty_queue(self):
        # empty queue
        ret = ''
        while not self.queue.empty():
                ret = f'{ret}{self.queue.get_nowait()}'
                self.queue.task_done()
        return ret


class AudioFileReco(Reco):

    def __init__(self, logger,  filename, name='Mic Recognition thread', server='localhost:2700', loglevel='warning'):
        super().__init__(name=name, server=server,  logger=logger)
        self.filename = filename
        self.loglevel=loglevel

    async def hello(self, uri):
        async with websockets.connect(uri) as websocket:
            # launch ffmpeg
            proc = await asyncio.create_subprocess_exec(
                           'ffmpeg', '-nostdin', '-loglevel', self.loglevel, '-i', self.filename,
                           '-ar', f'{self.RATE}', '-ac', '1', '-f', 's16le', '-',
                           stdout=asyncio.subprocess.PIPE)
                           
            while not self.shutdown_flag.is_set():
                data = await proc.stdout.read(self.CHUNK)

                if len(data) == 0:
                    break

                await websocket.send(data)
                response = await websocket.recv()
                json_response = json.loads(response)
                if 'text' in json_response:
                    self.queue.put(f'{json_response.get("text")}\n')

            await websocket.send('{"eof" : 1}')
            response = await websocket.recv()
            json_response = json.loads(response)
            if 'text' in json_response:
                self.queue.put(f'{json_response.get("text")}\n')
            
            await proc.communicate()

    def run(self):
        asyncio.run(self.hello(f'ws://{self.server}'),   debug=True)
        

class MicReco(Reco):   
    
    def __init__(self, logger,  name='Mic Recognition thread', server='localhost:2700'):
        super().__init__(name=name, server=server,  logger=logger)

    @contextmanager
    def _pyaudio(self) -> Generator[PyAudio, None, None]:
        p = PyAudio()
        try:
            yield p
        finally:
            self.logger.info('Terminating PyAudio object')
            p.terminate()

    @contextmanager
    def _pyaudio_open_stream(self, p: PyAudio, *args, **kwargs) -> Generator[Stream, None, None]:
        s = p.open(*args, **kwargs)
        try:
            yield s
        finally:
            self.logger.info('Closing PyAudio Stream')
            s.close()

    @asynccontextmanager
    async def _polite_websocket(self, ws: websockets.WebSocketClientProtocol) -> AsyncGenerator[websockets.WebSocketClientProtocol, None]:
        try:
            yield ws
        finally:
            self.logger.info('Terminating connection')
            await ws.send('{"eof" : 1}')
            response = await ws.recv()
            json_response = json.loads(response)
            if 'text' in json_response:
                self.queue.put(f'{json_response.get("text")}\n')

    async def hello(self, uri):
        async with AsyncExitStack() as stack:
            ws = await stack.enter_async_context(websockets.connect(uri))
            self.logger.info(f'Connected to {uri}')
            self.logger.info('Type Ctrl-C to exit')
            ws = await stack.enter_async_context(self._polite_websocket(ws))
            p = stack.enter_context(self._pyaudio())
            s = stack.enter_context(self._pyaudio_open_stream(p,
                format = self.FORMAT, 
                channels = self.CHANNELS,
                rate = self.RATE,
                input = True, 
                frames_per_buffer = self.CHUNK))
            while not self.shutdown_flag.is_set():
                data = s.read(self.CHUNK)
                if len(data) == 0:
                    break
                await ws.send(data)
                response = await ws.recv()
                json_response = json.loads(response)
                if 'text' in json_response:
                    self.queue.put(f'{json_response.get("text")}\n')

    def run(self):           
        try:
            asyncio.set_event_loop(asyncio.new_event_loop())
            loop = asyncio.get_event_loop()
            loop.run_until_complete(
                self.hello(f'ws://{self.server}'))
        except Exception as e:
            loop.stop()
            loop.run_until_complete(loop.shutdown_asyncgens())
            self.logger.error(f'Oops {e}')
            exit(1)
        finally:
            loop.stop()
            loop.run_until_complete(loop.shutdown_asyncgens())
            self.logger.info('Bye')
            exit(0)


class Application(tkinter.Frame):
    
    def __init__(self, logger,  master=None, server='localhost:2700',  audiodir='~/Recording'):
        super().__init__(master)
        self.master = master
        self.server = server
        self.master.protocol('WM_DELETE_WINDOW', self.quit)
        self.master.title('Dictée Vocale')
        self.logger= logger
        self.audiodir = audiodir
        self.create_widgets()
        self.pack()
    
    def create_popup_menu(self, widget):
        # create contextual menu
        self.popup_menu = tkinter.Menu(widget, tearoff=0)
        self.popup_menu.add_command(label='Selectionner Tout', accelerator='Ctrl + a', command=lambda: self.select_all(widget))
        self.popup_menu.insert_separator(1)
        self.popup_menu.add_command(label='Couper', accelerator='Ctrl + x', command=lambda: self.cut(widget))
        self.popup_menu.add_command(label='Copier', accelerator='Ctrl + c', command=lambda: self.copy(widget))
        self.popup_menu.add_command(label='Coller', accelerator='Ctrl + v', command=lambda: self.paste(widget))
        self.popup_menu.insert_separator(5)
        self.popup_menu.add_command(label='Annuler', accelerator='Ctrl + z', command=lambda: self.undo(widget))
        self.popup_menu.add_command(label='Rétablir', accelerator='Ctrl + Shift + z', command=lambda: self.redo(widget))
    
    def context_menu(self, event):
        # set contextual menu
        try:
            self.popup_menu.tk_popup(event.x_root, event.y_root, 0)
        finally:
            self.popup_menu.grab_release()
    
    def copy(self, widget):
        widget.event_generate('<<Copy>>')

    def paste(self, widget):
        widget.event_generate('<<Paste>>')

    def cut(self, widget):
        widget.event_generate('<<Cut>>')

    def select_all(self, widget):
        widget.tag_add('sel','1.0','end')
    
    def undo(self, widget):
        widget.event_generate('<<Undo>>')

    def redo(self, widget):
        widget.event_generate('<<Redo>>')  
    
    def create_widgets(self):
    
        # Creating scrolled text area widget 
        self.text_area = tkinter.scrolledtext.ScrolledText(
            self.master,
            wrap=tkinter.WORD,
            width =80,
            height=20,
            font=tkinter.font.Font(family='Helvetica', size=15),
            undo=True,
        )
        self.text_area.pack(padx=10, pady=10, fill=tkinter.BOTH, expand=True)
        
        self.create_popup_menu(self.text_area)
        self.text_area.bind('<Button-3>', self.context_menu)
        
        # Creating frame
        self.f = tkinter.Frame(self.master)
        self.f.pack(fill=tkinter.BOTH, expand=True)
        
        # Creating left label frame
        self.lf = tkinter.LabelFrame(
            self.f,
            text='Microphone',
            font=tkinter.font.Font(family='Helvetica', size=15)
        )
        self.lf.pack(side=tkinter.LEFT, padx=10, pady=0)      
        
        # Creating mic toggle
        self.toggle_mic_btn = tkinter.Button(
            self.lf,
            text='OFF',
            command=self.toggle_mic,
            relief=tkinter.RAISED,
            state=tkinter.NORMAL,
            fg='red',
            font=tkinter.font.Font(family='Helvetica', size=12, weight='bold')
            #activebackground='red',
        )
        self.toggle_mic_btn.pack(side=tkinter.TOP, padx=5, pady=5)
        
        # right label frame
        self.rf = tkinter.LabelFrame(
            self.f,
            text='Fichier Audio',
            font=tkinter.font.Font(family='Helvetica', size=15)
        )       
        self.rf.pack(side=tkinter.LEFT, padx=10, pady=0)
        
        # Creating Open file button
        self.audiofile_open_btn = tkinter.Button(
            self.rf,
            text='Ouvrir',
            font=tkinter.font.Font(family='Helvetica', size=12, weight='bold'),
            command=self.open_audiofile,
            #activebackground='red',
        )
        self.audiofile_open_btn.pack(side=tkinter.LEFT, padx=5, pady=5)
        
        # Creating Stop button
        self.audiofile_stop_btn = tkinter.Button(
            self.rf,
            text='Stop',
            font=tkinter.font.Font(family='Helvetica', size=12, weight='bold'),
            command=self.stop_audiofile,
            #activebackground='red',
        )
        self.audiofile_stop_btn['state'] = tkinter.DISABLED
        self.audiofile_stop_btn.pack(side=tkinter.RIGHT, padx=5, pady=5)        
        
        # Creating Exit button
        self.quit_btn = tkinter.Button(
            self.master,
            text='Exit',
            # fg='red',
            command=self.quit,
            font=tkinter.font.Font(family='Helvetica', size=12)
        )
        self.quit_btn.pack(side=tkinter.RIGHT, padx=10, pady=10)
    
    def toggle_mic(self):
        if self.toggle_mic_btn.config('text')[-1] =='ON':
            # if mic is on, stop it
            self.stop_mic()
        else:
            # if mic is off, start it
            self.start_mic()
            
    def start_mic(self):
        # disble other buttons
        self.audiofile_open_btn['state'] = tkinter.DISABLED
        self.audiofile_stop_btn['state'] = tkinter.DISABLED
        
        # start mic in new thread
        self.mic_thread =  MicReco(server=self.server,  logger=self.logger)
        self.mic_thread.start()
        
        # start to monitor mic thread
        self.monitor_thread(self.mic_thread)
        
        # toggle mic button
        self.toggle_mic_btn.config(text='ON', fg='green', activeforeground='green', relief=tkinter.SUNKEN)
        
        # set cursor as busy
        self.master.config(cursor='watch')
        self.text_area.config(cursor='watch')
    
    def stop_mic(self):
        try:
            self.stop_thread(self.mic_thread)
        except(AttributeError) as e:
            self.logger.debug(f'AttributeError ignored : {e}')

    def open_audiofile(self):
        # open filedialog
        audiofilename = tkinter.filedialog.askopenfilename(parent=self.master, initialdir=self.audiodir)
     
        # disable buttons
        self.toggle_mic_btn['state'] = tkinter.DISABLED
        self.audiofile_open_btn['state'] = tkinter.DISABLED
        
        # enable Stop button
        self.audiofile_stop_btn['state'] = tkinter.NORMAL
        
        # set cursor as busy
        self.master.config(cursor='watch')
        self.text_area.config(cursor='watch')
        
        # start to read file in new thread
        self.audiofile_thread = AudioFileReco(server=self.server, filename=audiofilename, logger=self.logger)
        self.audiofile_thread.start()
        
        # start to monitor mic thread
        self.monitor_thread(self.audiofile_thread)

    def stop_audiofile(self):
        try:
            self.stop_thread(self.audiofile_thread)
        except(AttributeError) as e:
            # self.audiofile_thread is not defined
            self.logger.debug(f'AttributeError ignored : {e}')

    def monitor_thread(self,  thread):
        # show queue's content in text area
        self.text_area.insert(tkinter.INSERT, thread.cleanly_empty_queue())
        
        if thread.is_alive():
            # if thread is running, continue to monitor it (every 100ms)
            self.monitoring = self.master.after(100, lambda: self.monitor_thread(thread))
        else:
            # if thread is finished, reninit all buttons
            self.init_buttons()

    def stop_thread(self,  thread):
        try:
            # stop monitoring thread
            self.master.after_cancel(self.monitoring)
        except(AttributeError) as e1:
            # self.monitoring is not defined
            self.logger.debug(f'AttributeError ignored : {e1}')
        finally:
            try:
                self.text_area.insert(tkinter.INSERT, thread.cleanly_stop())
            except(AttributeError) as e2:
                # thread is not defined
                self.logger.debug(f'AttributeError ignored : {e2}')
            finally:
                # when thread is finished, reninit all buttons
                self.init_buttons()

    def init_buttons(self):
        # set buttons and cursor in their initial states
        self.audiofile_open_btn['state'] = tkinter.NORMAL
        self.audiofile_stop_btn['state'] = tkinter.DISABLED
        self.toggle_mic_btn['state'] = tkinter.NORMAL
        self.toggle_mic_btn.config(text='OFF', fg='red', activeforeground='red', relief=tkinter.RAISED)
        self.master.config(cursor='')
        self.text_area.config(cursor='')
                    
    def quit(self):
        # open quit dialog
        if tkinter.messagebox.askokcancel('Quitter', 'Voulez-vous quitter?'):
            self.stop_mic()
            self.stop_audiofile()
            self.master.destroy()


class SplashScreen(tkinter.Frame):

    def __init__(self, master=None, title=None):
        super().__init__(master)
        
        self.master=master
        self.pack()
        
        # Adjust size
        self.master.geometry('400x100')
        
        # Set as splash screen
        self.master.wm_attributes('-type', 'splash')
      
        # Set Label 
        tkinter.Label(self.master, text=title, font=18).pack()
        
        # Progress Bar
        self.pbar = tkinter.ttk.Progressbar(self.master, mode='indeterminate', orient=tkinter.HORIZONTAL)
        self.pbar.pack(fill=tkinter.BOTH, expand=True, padx=25, pady=30)
        self.pbar.start(10)

class SplashScreenWithExit(SplashScreen):
    
    def __init__(self, container, master=None, title=None):
        super().__init__(master, title)
        self.container=container
        self.master.geometry('400x150')
        
        # creating exit button
        self.quit_btn = tkinter.Button(self.master, text='Exit', command=self.quit)
        self.quit_btn.pack(side=tkinter.RIGHT, padx=10, pady=10)

    def quit(self):
        # open quit dialog
        if tkinter.messagebox.askokcancel('Quitter', 'Voulez-vous quitter?'):
            try:
                # if exit, stop monitoring container ("docker start" cmd)
                self.master.after_cancel(self.container.start_monitoring)
            except(AttributeError) as e1:
                # self.container.start_monitoring is not defined
                self.logger.debug(f'AttributeError ignored : {e1}')
            finally:
                try:
                    # if exit, stop monitoring connection to container (via websockets)
                    self.master.after_cancel(self.container.connection_monitoring)
                except(AttributeError) as e2:
                    # self.container.connection_monitoring is not defined
                    self.logger.debug(f'AttributeError ignored : {e2}')
                finally:
                    # stop progress bar
                    self.pbar.stop()
                    # close spash screen
                    self.master.destroy()
                    # stop docker
                    self.container.stop()
                    # exit with an error code
                    exit(1)
    
class DockerContainer:
    def __init__(self, id, logger,  server='localhost:2700'):
        self.id = id
        self.server = server
        self.logger = logger
        
    def test_uri(self, uri):
        # test if server is online
        async def inner():
            try:
                ws = await websockets.client.connect(uri=uri)
            except (websockets.exceptions.InvalidHandshake ) as e1:
                self.logger.debug(f'Ignoring InvalidHandshake Exception ({e1})')
                return False
            except(Exception) as e2:
                self.logger.warning(f'Ignoring Exception ({e2})')
                return False
            else:
                await ws.close()
                return True;
        return asyncio.run(inner())

    def wait_connection(self):
        # wait for connection to server
        if  self.test_uri(uri=f'ws://{self.server}'):
            self.logger.info('Connection OK')
            # if connection is OK, stop progress bar and close splash screen
            self.start_splash_screen.pbar.stop()
            self.start_splash_screen.master.destroy()
        else:
            self.logger.info('Waiting for connection')
            # if connection fails, retest it later
            self.connection_monitoring = self.start_splash_screen.master.after(500, self.wait_connection)

    def monitor_start(self, cmd_ret):
        # get command return code
        return_code = cmd_ret.poll()
        if return_code is None:
            # if command is not finished, continue to monitor it
            self.start_monitoring = self.start_splash_screen.master.after(100, lambda: self.monitor_start(cmd_ret))
        elif return_code == 0:
            # if command is successful, quit splash screen mainloop
            self.start_splash_screen.master.quit()
        else:
            # if error, stop progress bar, close splash screen, show an error and exit with an error code
            self.start_splash_screen.pbar.stop()
            self.start_splash_screen.master.destroy()
            tkinter.messagebox.showerror(title='Erreur', message='Impossible de démarrer le container docker')
            exit(1)
    
    def start(self):
    
        # creating tkinter splash screen
        self.start_splash_screen = SplashScreenWithExit(container=self, master= tkinter.Tk(), title='Démarrage de la reconnaissance vocale')
        
        # launch container and montitor its start
        ret = subprocess.Popen(['docker', 'start' , self.id])        
        self.monitor_start(ret)
        
        # show splash screen
        self.start_splash_screen.mainloop()     
        
        # waiting for connection to server
        self.wait_connection()
        
        # show splash screen        
        self.start_splash_screen.mainloop()     

    def monitor_stop(self, ret):
        # get command return code
        cmd_code = ret.poll()
        if cmd_code == 0:
            # if command is successful, stop prgressbar and close splash screen
            self.stop_splash_screen.pbar.stop()
            self.stop_splash_screen.master.destroy()
        elif cmd_code is None:
            # if command is not finished, continue to monitor it
            self.stop_splash_screen.master.after(100, lambda: self.monitor_stop(ret))
        else:
            # if error, stop progress bar, close splash screen, show an error and exit with an error code
            self.stop_splash_screen.pbar.stop()
            self.stop_splash_screen.master.destroy()
            tkinter.messagebox.showerror(title='Erreur', message='Impossible de stopper le container docker')
            exit(1)

    def stop(self):
        
        # tkinter splash screen
        self.stop_splash_screen = SplashScreen(master=tkinter.Tk(), title='Extinction de la reconnaissance vocale')
        
        # stop container and montitor its stop
        ret = subprocess.Popen(['docker', 'stop' , self.id])
        self.monitor_stop(ret)
        
        # show splash screen
        self.stop_splash_screen.mainloop(),

def valid_host(arg_value):
    matches = re.fullmatch(r'^(?:(?P<ip>\[?(?:[0-9A-F]{0,4}:){0,7}(?:[0-9A-F]{0,4})\]?)|(?P<hostname>[\w\.\-]+))(?:\:(?P<port>[\d]{1,5})){0,1}$',  arg_value,  re.I)
    if not matches:
        raise argparse.ArgumentTypeError('Invalid server address')
    if not matches.group('hostname') and not matches.group('ip'):
        raise argparse.ArgumentTypeError('Invalid hostname/ip')
    if matches.group('port') and (int(matches.group('port'))<0 or int(matches.group('port'))>65535):
        raise argparse.ArgumentTypeError('Invalid port')
    return arg_value

if __name__ == '__main__':

    # creating args parser
    main_parser = argparse.ArgumentParser(description='Simple GUI for vosk')
    main_parser.add_argument('-s', '--server', type=valid_host,  default=SERVER, help=f'Server (default: {SERVER})')
    main_parser.add_argument('-c', '--container', default=DOCKER_CONTAINER_ID, help=f'Docker container id (default: {DOCKER_CONTAINER_ID})')
    main_parser.add_argument('-d', '--dir',  default=AUDIO_DIR,  help=f'Audio directory (default: {AUDIO_DIR})')
    
    # log group args
    log_group = main_parser.add_argument_group('logs')
    levels = ('DEBUG', 'INFO', 'WARNING', 'ERROR', 'CRITICAL')
    log_group.add_argument('-l', '--loglevel', default='WARNING', choices=levels, help='Log level (default: "WARNING")')
    log_group.add_argument('-m', '--mainloglevel', default='WARNING', choices=levels, help='Main log level (default: "WARNING"})')
 
    # parse args
    args = main_parser.parse_args()
    
    # set default logging
    numeric_level = getattr(logging, args.loglevel.upper(), None)
    if not isinstance(numeric_level, int):
        raise ValueError(f'Invalid log level: {args.loglevel}')
    logging.basicConfig(level=numeric_level)

    # set main logger
    main_numeric_level = getattr(logging, args.mainloglevel.upper(), None)
    if not isinstance(main_numeric_level, int):
        raise ValueError(f'Invalid log level: {args.mainloglevel}')
    main_logger = logging.getLogger(__name__)
    main_logger.setLevel(main_numeric_level)

    # ensure only on instance can run
    pid_file = os.path.join('/var/lock', f'{pathlib.PurePosixPath(__file__).stem}.lock')
    fp = open(pid_file, 'w')
    try:
        fcntl.lockf(fp, fcntl.LOCK_EX | fcntl.LOCK_NB)
    except IOError:
        logging.error('Another instance is running')
        exit(1)

    # creating docker container
    container = DockerContainer(id=args.container, server=args.server,  logger=main_logger)

    # start container
    container.start()

    # tkinter application
    app = Application(master=tkinter.Tk(), server=args.server, logger=main_logger,  audiodir=args.dir)

    # start GUI
    app.mainloop()

    # stop container
    container.stop()
