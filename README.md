# Vosk Gui

A simple vosk GUI

## Install dependencies

Install docker, ffmpeg, python3, websockets, PyAudio and  tkinter.

On Ubuntu 20.04 :
```bash
sudo apt install docker.io ffmpeg python3-websockets python3-pyaudio python3-tk
```

## vosk docker container

Run vosk docker container.

On Ubuntu 20.04 :
```bash
docker run -d -p 2700:2700  alphacep/kaldi-fr:latest
```

Customize *vosk-gui.py* with the docker **container id** (use `docker ps -a` to retrieve it)

## .desktop file

Customize *vosk-gui.desktop* file (Name, Comment, Exec and Icon fieds), then copy it to your *~/.local/share/applications/* directory.

Alternately, you can copy *vosk-gui.desktop* file to */usr/local/share/applications/* directory.

On Ubuntu 20.04 :

```bash
sudo mkdir -p /usr/local/share/applications
sudo cp vosk-gui.desktop /usr/local/share/applications
sudo chmod +x vosk-gui.py
sudo cp vosk-gui.py /usr/local/bin
```
